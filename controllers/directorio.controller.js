const DB = require('./../services/dbServices');
const Utils = require('./../services/utils');
const us = require('./../services/usuario.services');
const ds = require('./../services/directorio.service');

function getDirectorio(req, res){
    DB.directorio.findOne({
        where:{id:id}
    }).then(user =>{
        return user;
    }).catch(ex=>{
        console.log(ex);
        return null;
    });
}

function getAllDirectorio(req, res){

    var idUser = req.body.userid;

    var user = us.devolverUserId(idUser);

    DB.directorio.findAll({
        usuario:user
    }).then(direc=>{
        res.status(200).jsonp(Utils.devolverDatos(0,direc));
    }).catch(ex=>{
        res.status(200).jsonp(Utils.devolverDatos(-1,"Error " + ex.message));
        return null;
    });
}

function editDirectorio(req, res){
    var id = req.body.id;

    var direct = req.body.direc;

    DB.directorio.update(direct,{
        where:{
            id:id
            }
    }).then(result => {
        console.log(result);
    })
    .catch(ex =>{
        console.log(ex);
    });

    res.status(200).jsonp(Utils.devolverDatos(0,""));
}

function deleteDirectorio(req, res){
    var id = req.body,id;

    var direct = ds.devolverDirectorioId(id);

    if(direct !== null)
        direct.destroy().then(result =>{
            console.log(ex);
        }).catch(ex =>{
            console.log(ex);
        })

    res.status(200).jsonp(Utils.devolverDatos(0,""));
}

module.exports = function(router) {
    router.get("/directorio/view", getDirectorio);
    router.get("/directorio/all", getAllDirectorio);
    router.post("/directorio/save", editDirectorio);
    router.delete("/directorio/delete", deleteDirectorio);
};