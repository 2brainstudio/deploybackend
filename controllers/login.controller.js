const crypto = require('crypto');
const DB = require('./../services/dbServices');
const parameters = require('./../config/parameters');
const Utils = require('./../services/utils');
const util = require('util');
const Sequelize = require('sequelize');
const jwt = require('jsonwebtoken');
const Op = Sequelize.Op;
const fs = require('fs');

const email = require("./../services/email.services");

var us = require('./../services/usuario.services');

// var cert = fs.readFileSync('keyrs256.key');
//
//
// // encripta la contrasena del usuario usando un algoritmo sha512
// function crearClave(pass){
//     const key = crypto.pbkdf2Sync(pass, parameters.tokenSecret, 10, 100, 'sha512');
// ;
//
//     return key.toString('hex');
// }
//
// function crearToken(user){
//
//     var token = "";
//
//     var dtToken ={
//         cliente: user.dataValues.cliente,
//         canal: user.dataValues.canal,
//         exp: Math.floor(Date.now() / 1000 ) + (60 * 60)
//     }
//
//     try {
//         token = jwt.sign(dtToken, cert, {algorithm: 'RS256'});
//     }
//     catch(ex){
//         console.log(ex);
//     }
//
//
//     return Utils.devolverDatos(0,token)
//
//
// }


function login(req, res){


    var username = req.body.username;
    var password = req.body.password;


    loguearUsuario(username, password)
        .then(result => {


            us.crearToken(result).then(dt => {
                res.status(200).jsonp(dt);
            }).catch(ex => {
                res.status(200).jsonp(ex);
            });


        })
        .catch(error =>{
            res.status(200).jsonp(error);
        });

}

function recovery(req, res){

}



function loguearUsuario(username, password){

    return new Promise((resolve, reject)=>{
        var passHash = us.crearClave(password);
        DB.usuario.findOne({
            where:{username: username, password: passHash}
            // where:{username: username}
        }).then(user=>{
            // console.log(user);
            if(user === null)
                return reject(Utils.devolverDatos(-1, {message:"El usuario  o contrasena son incorrectos"}));
            else {

                // console.log("cambiando clave " + user.dataValues.id);
                // user.password = passHash;
                // user.save().then(()=>{
                //     console.log("guardado");
                // })


                // DB.usuario.update(user, {
                //     where: {
                //         id: user.dataValues.id
                //     }
                // }).then(result => {
                //     console.log("resultado");
                //     console.log(result);
                // })
                // .catch(ex => {
                //     console.log("error");
                //     console.log(ex);
                // });

                // return resolve({envioEmail:true, email:user.dataValues.email});

                /*if(user.dataValues.cambiopassword)
                {
                    enviarCorreoRestaurarContrasena(user);
                    return resolve({envioEmail:true, email:user.dataValues.email});
                }*/

                DB.clientes.findOne({
                    where: {usuario: username}
                }).then(cli => {
                    return resolve({cli:cli, user:user.dataValues});
                }).catch(ex => {
                    return reject(Utils.devolverDatos(-2, "Error " + ex.message));
                })
            }

        }).catch(ex =>{
            return reject(Utils.devolverDatos(-3,"Error " + ex.message));
        });
    });


}

function enviarCorreoRestaurarContrasena(user){
    email.enviarMailRecovery(user.dataValues.email);
}

function getTokenAuth(user){

}

function cambiarContrasena(req,res){


    // res.status(200).json(  "lol" );

    var id = req.body.id;
    var password = req.body.password;

    password = us.crearClave(password);

    DB.usuario.findOne({
        where: {id:id}
    }).then(user=>{
        user.password = password;
        user.cambiopassword = false;
        user.save().then(()=>{
            res.status(200).jsonp(Utils.devolverDatos(0,{}));
        });
    })
    .catch(ex=>{
        res.status(200).jsonp(Utils.devolverDatos(-1,ex));
    });

}


module.exports = {
    login: login,
    cambiarContrasena: cambiarContrasena
}