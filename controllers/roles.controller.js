var DB = require('./../services/dbServices');
const Utils = require('./../services/utils');

function getRole(req, res){

    var id = req.body.id;

    var rol = devolverRolId(id);

    if(rol !== null)
        res.status(200).jsonp(Utils.devolverDatos(0,rol));
    else
        res.status(200).jsonp(Utils.devolverDatos(-1,"No existe"));

}

function devolverRolId(id){
    DB.rol.findOne({
        where:{id:id}
    }).then(rol =>{
        return rol;
    }).catch(ex=>{
        console.log(ex)
        return null;
    });
}

function getAllRoles(req, res){
    DB.rol.findAll().then(roles=>{
        res.status(200).jsonp(Utils.devolverDatos(0,roles));
    }).catch(ex=>{
        res.status(200).jsonp(Utils.devolverDatos(-1,"error " + ex.message));
        return null;
    });
}

function editRole(req, res){
    var id = req.body.id;
    var rol = req.body.rol;

    DB.rol.update(rol,{
        where: {
            id: id
        }
    }).then(result =>{
        console.log(result);
    }).catch(ex =>{
        console.log(ex);
    })

    res.status(200).jsonp(Utils.devolverDatos(0,""));

}

function deleteRole(req, res){
    var id = req.body.id;

    var rol = devolverRolId(id);

    rol.destroy().then(result=>{
        console.log(result);
    }).catch(ex=>{
        console.log(ex);
    })

    res.status(200).jsonp(Utils.devolverDatos(0,""));
}

module.exports = function(router){
    router.get("/role/view", getRole);
    router.get("/role/all", getAllRoles);
    router.post("/role/edit",editRole);
    router.delete("/role/delete", deleteRole);
}