const DB = require('./../services/dbServices');
const Utils = require('./../services/utils');
const us = require('./../services/usuario.services');

function getUser(req, res){

    var id = req.body.id;

    var user = us.devolverUserId(id);

    if(user === null)
        Utils.devolverDatos(-1,"No existe el usuario");
    else
        res.status(200).jsonp(Utils.devolverDatos(0,user));

}


function getAllUser(req, res){
    DB.usuario.findAll().then(users=>{
        res.status(200).jsonp(Utils.devolverDatos(0,users));
    }).catch(ex=>{
        console.log(ex);
        res.status(200).jsonp(Utils.devolverDatos(-1,"Error " + ex.message));
    });
}

function getAllClientesSAP(req, res){
    DB.clientes.findAll().then(cli => {
        res.status(200).jsonp(Utils.devolverDatos(0,cli));
    }).catch(ex=>{
        console.log(ex);
        res.status(200).jsonp(Utils.devolverDatos(-1,"Error " + ex.message));
    })
}

function editUser(req, res){
    //DB.usuario.

    let id = req.body.id;
    let userData = req.body.user;
    let isNew = req.body.isNew;

    if(!isNew) {
        let user = devolverUserId(id);

        user.password = us.crearClave(user.password);

        DB.usuario.update(user, {
            where: {
                id: id
            }
        }).then(result => {
            console.log(result);
        })
            .catch(ex => {
                console.log(ex);
            })

        res.status(200).jsonp(Utils.devolverDatos(0, ""));
    }
    else{

    }

}

function deleteUser(req, res){
    var id = req.body.id;

    var user = devolverUserId(id);

    user.destroy().then(result =>{
        console.log(result);
    })
    .catch(ex =>{
        console.log(ex);
    })

    res.status(200).jsonp(Utils.devolverDatos(0,""));
}



// module.exports ={
//     getUser: getUser,
//     getAllUser: getAllUser,
//     editUser: editUser,
//     deleteUser: deleteUser
// };

module.exports = function(router){
    router.get("/usuario/id", getUser);
    router.get("/usuario/all", getAllUser);
    router.post("/usuario/save",editUser);
    router.delete("/usuario/delete", deleteUser);
    router.get("/usuario/clientes", getAllClientesSAP);
};