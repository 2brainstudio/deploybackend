module.exports = function(router){
    const userCtrl = require('./usuario.controller')(router);
    const roleCtrl = require('./roles.controller')(router);
    const direCtrl = require('./directorio.controller')(router);
};

