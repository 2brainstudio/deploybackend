module.exports = function(sequelize, Sequelize){
    const User = sequelize.define('usuario', {
        id:{
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        id_usuario: Sequelize.STRING(40),

        id_cliente: Sequelize.BIGINT,
        id_canal: Sequelize.BIGINT,

        username: {
            type: Sequelize.STRING(60),
            allowNull: false
        },
        password: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        salt: {
            type: Sequelize.STRING(255),
            allowNull: false
        },

        cambiopassword:{
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },

        escomercial:{
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },

        token: Sequelize.STRING(255),

    },{
        tableName: "comext_usuarios",
        timestamps: false
    });

    return User;
}