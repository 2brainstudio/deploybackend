

module.exports = function(sequelize, Sequelize){
    const Role = sequelize.define('rol', {
        id:{
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        nombre: {
            type: Sequelize.STRING(60),
            allowNull: false
        }

    },{
        tableName: "comext_roles",
        timestamps: false
    });

    return Role;
}