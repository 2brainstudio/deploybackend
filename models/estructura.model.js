const estructura  = require('./user.model');

module.exports = function(sequelize, Sequelize){
    const Estructura = sequelize.define('estructura', {
        org_venta:{
            type: Sequelize.STRING(4)
        },

        // canal:{
        //     type: Sequelize.STRING(2)
        // },

        almacen:{
            type: Sequelize.STRING(5)
        },

        centro:{
            type: Sequelize.STRING(4)
        },

        sociedad:{
            type: Sequelize.STRING(30)
        },

        id_funcionalidad:{
            type: Sequelize.BIGINT
        },


        // id:{
        //    type: Sequelize.BIGINT
        // }

    },{
        tableName: "estructura_sd",
        timestamps: false
    });

    return Estructura;
}

