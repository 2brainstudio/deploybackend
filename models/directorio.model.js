module.exports = function(sequelize, Sequelize){
    const Directorio = sequelize.define('directorio', {
        id:{
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        email: {
            type: Sequelize.STRING(50),
            allowNull: false
        },

        nombre: {
            type: Sequelize.STRING(50),
            allowNull: false
        },

        reportenovhabilitado:{
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },

        reportegensp:{
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },


    },{
        tableName: "comext_directorio",
        timestamps: false
    });

    return Directorio;
}