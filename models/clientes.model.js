const User = require('./user.model');

module.exports = function(sequelize, Sequelize){
    const ClientesSap = sequelize.define('clientes', {
       cliente:{
           type: Sequelize.STRING(10)
       },

        destinatario:{
           type: Sequelize.STRING(10)
        },

        usuario:{
           type: Sequelize.STRING(40)
        },

        nombre_destinatario:{
            type: Sequelize.STRING(70)
        },

        nombre_cliente:{
            type: Sequelize.STRING(70)
        },

        canal:{
           type: Sequelize.STRING(2)
        }

        // id:{
        //    type: Sequelize.BIGINT
        // }

    },{
        tableName: "cliente_sap",
        timestamps: false
    });

    return ClientesSap;
}

