module.exports = function(sequelize, Sequelize) {
    const Codigo = sequelize.define('codigo', {
        id:{
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        tipo: {
            type: Sequelize.INTEGER(),
            allowNull: false,
        },

        codigo: {
            type: Sequelize.STRING(5),
            allowNull: false
        },

        descripcion:{
            type: Sequelize.STRING(255),
            defaultValue: true
        },



    },{
        tableName: "comext_codigos",
        timestamps: false
    });

    return Codigo;
}