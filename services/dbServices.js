const Sequelize = require('sequelize');

const parameters = require('./../config/parameters');

const secretKey = "ThisTokenIsNotSoSecretChangeIt";



const sequelize = new Sequelize(parameters.database, parameters.username, parameters.password,{
   host: parameters.dbhost,
   dialect:'postgres',
   pool:{
       max: 5,
       min: 0,
       idle: 10000
   }
});

const clientes = require("./../models/clientes.model")(sequelize, Sequelize);
const usuario = require("./../models/user.model")(sequelize, Sequelize);
const rol = require("./../models/role.model")(sequelize, Sequelize);
const directorio = require("./../models/directorio.model")(sequelize, Sequelize);
const codigos = require("./../models/codigos.model")(sequelize, Sequelize);
const estructura = require("./../models/estructura.model")(sequelize, Sequelize);

usuario.belongsToMany(rol,{ as: 'roles', through:"comext_user_role", timestamps:false, foreignKey:'user_id'});
rol.belongsToMany(usuario,{ as: 'users', through:"comext_user_role", timestamps:false, foreignKey:'role_id'});

usuario.belongsToMany(directorio, {as: 'directorio', through:"comext_directorio_x_cliente", timestamps: false, foreignKey:"user_id"});
directorio.belongsToMany(usuario, {as: 'usuario', through:"comext_directorio_x_cliente", timestamps: false, foreignKey:"id_directorio"})





function importarModelos(){
    sequelize.sync().then(function(){
        console.log("sin problemas");
        // clientes.findAll().then( cli =>{
        //
        //     cli.forEach(c =>{
        //         console.log(c.dataValues);
        //     })
        // })
        //
        // usuario.findAll().then(user =>{
        //     user.forEach( u=>{
        //         console.log(u.dataValues);
        //     })
        // })
    }).catch(function(err){
        console.log(err, 'Error cuando se actualiza la base de datos')
    });


}

function nextDoc(req, res){
    sequelize.query("SELECT nextval('sp1_seq')").spread((result, metadata)=>{
        res.status(200).jsonp({cons:result[0].nextval});
    });
}

module.exports = {
    main: sequelize,
    importar: importarModelos,
    usuario: usuario,
    clientes: clientes,
    directorio: directorio,
    rol: rol,
    codigos: codigos,
    estructura: estructura,
    nextDoc: nextDoc
}