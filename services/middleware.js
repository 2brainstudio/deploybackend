const soap = require('soap');

const jsonxml = require('jsontoxml');
const parameter = require("./../config/parameters");

const url = parameter.urlBus;

const DB = require("./../services/dbServices");

const email = require("./../services/email.services");


var c = null;

var requestHeader = {
    "client":"local",
    "ipAdress":"localhost",
    "userName":"user1",
    "sessionID":12345,
    "requestID":123456,
    "activeRecord":1
};




crearClienteSoap();

function crearClienteSoap(){
    soap.createClient(url,(err, client) => {

        if(err == undefined || err == null)
            if(c == null)
                c = client;


    });
}

function consumirMateriales(req, res){

    var cliente = req.body.cliente;
    var canal = req.body.canal;
    


    if(cliente == undefined || canal == undefined)
    {
        res.status(301).jsonp({message:"Faltan algunos parametros para la consulta"});
    }
    else {
        var requestBody = {
            numDebtor: cliente,
            channel: canal,
            salesOrg: "4000"
        };


        if (c == null) {
            res.status(301).jsonp({message: "error al crear el servicio soap"});
            crearClienteSoap();
        }
        else {

            c.getMaterialList({
                "requestHeader": requestHeader,
                "requestBody": requestBody
            }, (err, result, raw, soapHeader) => {
                console.log(err);
                res.status(200).jsonp(result.return.responseBody.productosXClienteList);
            });
        }
    }

}

function enviarSalesOrder(req, res){
    var datosBody = req.body.datos;

    // let canal = 43;
    let cliente = req.body.cliente;

    console.log(req.body);

    // var xml = jsonxml({
    //     "sal:salesOrders"
    //     "requestHeader": requestHeader,
    //     "requestBody": datosBody
    // });

    var xml = jsonxml({
        parent:[
            {name:"sal:salesOrders", attrs:{"xmlns:sal":"http://com.redhat.fedecafe.ws/sales-other-chanels-service"}, children:{
        "requestHeader": requestHeader,
            "requestBody": datosBody
    }}
        ]
    });

    xml = xml.replace("<parent>","");
    xml = xml.replace("</parent>","");

    // res.status(200).jsonp({_xml: xml});

    if(cliente == undefined)
    {
        res.status(301).jsonp({message:"Faltan algunos parametros para la consulta"});
    }
    else{
        if (c == null) {
            res.status(301).jsonp({message: "error al crear el servicio soap"});
            crearClienteSoap();
        }
        else{
            c.salesOrders({_xml:xml}, (err, result, raw, soapHeader)=>{

                // console.log(err);
                console.log(err);
                if(err == null) {
                    let list = result.return.responseBody.summaryOrderList.summaryOrder;

                    if(list == null)
                        enviarCorreo(cliente, result.return.responseBody.summaryOrderList.summaryOrder);
                    else{
                        enviarCorreo(cliente, result.return.responseBody.summaryOrderList.summaryOrder);
                    }
                }
                //console.log(result);
                res.status(200).jsonp({xml:xml, result:result, error:err});
            })


        }
    }
}



function enviarCorreo(cliente, respuesta){
    DB.clientes.findOne({
        where: {cliente: cliente}
    }).then(cli => {

        DB.usuario.findOne({
            where: {username:cli.usuario}
        }).then(us =>{

            us.getDirectorio().then(direc=>{
                console.log("lol");
                // console.log(us);

                let correos = [];

                for(let e of direc){
                    correos.push(e.dataValues.email);
                }
                console.log(us.dataValues.username);
                email.enviarMail(correos, us.dataValues.username);
            })
            // email.enviarMail({
            //     from: 'Juan Valdez <juanvealdez@juanvaldez.com>',
            //     to
        }).catch(ex =>{
            console.log(ex);
        })

    }).catch(ex =>{
        console.log(ex);
    })
}

function devolverCodigos(req, res){
    DB.codigos.findAll().then(cod=>{
        res.status(200).jsonp(cod);
    })
}


module.exports ={
    consumirMateriales: consumirMateriales,
    enviarOrden: enviarSalesOrder,
    devolverCodigos: devolverCodigos
}