const DB = require('./dbServices');

function devolverDirectorioId(id){
    DB.directorio.findById(id)
        .then(d => {
            return d;
        }).catch(ex =>{
            return null;
    })
}

module.exports = {
    devolverDirectorioId: devolverDirectorioId
};