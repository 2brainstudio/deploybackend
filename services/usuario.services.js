const crypto = require('crypto');
const parameters = require('./../config/parameters');
const Utils = require('./../services/utils');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const DB = require('./dbServices');

var cert = fs.readFileSync('keyrs256.key');


// encripta la contrasena del usuario usando un algoritmo sha512
function crearClave(pass){
    const key = crypto.pbkdf2Sync(pass, parameters.tokenSecret, 10, 100, 'sha512');
    ;

    return key.toString('hex');
}

function crearToken(dt){

    var user = dt.cli;
    var login = dt.user;

    return new Promise((resolve, reject)=>{

        consultarParametrosXSales().then(datos =>{
            let token = "";
            var dtToken ={
                cliente: user.dataValues.cliente,
                canal: user.dataValues.canal,
                orgVenta: datos.orgVenta,
                almacen: datos.almacen,
                centro: datos.centro,
                cambioPass: login.cambiopassword,
                idUs: login.id,
                destinatario: user.dataValues.destinatario,
                exp: Math.floor(Date.now() / 1000 ) + (60 * 60)
            };

            try {
                token = jwt.sign(dtToken, cert, {algorithm: 'RS256'});
            }
            catch(ex){
                reject(ex);
            }
            resolve(Utils.devolverDatos(0,token));
        })
        .catch(ex=>{
            reject(Utils.devolverDatos(-1, "error"));
        });
    });




}

function consultarParametrosXSales(){
    return new Promise((resolve, reject)=>{
        DB.estructura.findOne({
            where: {id_funcionalidad: 1}
        }).then(es =>{
            let datos = {};
            datos.canal = es.dataValues.canal;
            datos.orgVenta = es.dataValues.org_venta;
            datos.almacen = es.dataValues.almacen;
            datos.centro = es.dataValues.centro;
            resolve(datos);
        }).catch(ex=>{
            console.log(ex);
            reject(Utils.devolverDatos(-1,"Error " + ex.message));
        });
    });




}

function devolverUserId(id){
    DB.usuario.findById(id).then(user =>{
        return user;
    }).catch(ex=>{
        console.log(ex);
        return null;
    });
}

module.exports = {
    crearClave: crearClave,
    crearToken: crearToken,
    devolverUserId: devolverUserId
}