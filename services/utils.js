function devolverDatos(codigo, datos){

    // console.log("llamando devolver datos");

    var dt = {
        codigo: codigo,
        datos: datos
    };

    // console.log(dt);

    return dt;
}

console.log("llamando devolver datos");

module.exports = {
    devolverDatos: devolverDatos
}