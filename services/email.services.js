const parameters = require('../config/parameters');
const nodemailer = require('nodemailer');

let transporter = null;
// nodemailer.createTestAccount((err, account)=>{
    transporter = nodemailer.createTransport({
        service: parameters.mailService,
        auth: parameters.mailAuth
    });
// });

function enviarMail(emails, user){

    if(parameters.mailDebug){
        var mailOptions = {
            from: parameters.mailAuth.user,
            to: 'jofunu@outlook.com',
            subject: 'Generado documentos',
            html: '<div>Buen dia</div>' +
            '<div>Los documentos --- han sido registrados por el usuario'+user+'</div>'
        }
    }
    else {
        var mailOptions = {
            from: parameters.mailAuth.user,
            to: emails,
            subject: 'Generado documentos',
            html: '<div>Buen dia</div>' +
                '<div>Los documentos --- han sido registrados por el usuario'+user+'</div>'
        }
    }

    transporter.sendMail(mailOptions, (error, info)=>{
        if(error)
            return console.log(error);

        console.log("Mensaje enviado: %s", info.messageId);
    })
}

function enviarMailRecovery(email, token){

    var link = "http://pedidosinternacional.juanvaldezcafe.com/internacional/web/recovery/?token="+ token;

    if(parameters.mailDebug){
        var mailOptions = {
            from: parameters.mailAuth.user,
            to: 'jofunu@outlook.com',
            subject: 'Restaurar contraseña',
            html: '<div>Buen dia</div>' +
            '<div>A traves del siguiente link puede restaurar su contraseña</div>' + '</br>' +
            '<a href="'+token+'">Restaurar Contraseña</a>'
        }
    }
    else {
        var mailOptions = {
            from: parameters.mailAuth.user,
            to: email,
            subject: 'Generado documentos',
            html: '<div>Buen dia</div>' +
            '<div>A traves del siguiente link puede restaurar su contraseña</div>' + '</br>' +
            '<a href="'+token+'">Restaurar Contraseña</a>'
        }
    }

    // console.log(mailOptions);
    // console.log(transporter);
    transporter.sendMail(mailOptions, (error, info)=>{
        if(error)
            return console.log(error);

        console.log("Mensaje enviado: %s", info.messageId);
    })
}

module.exports = {
    enviarMail: enviarMail,
    enviarMailRecovery: enviarMailRecovery
};


