var express = require('express'),
    cors = require('cors'),
    app = express(),
    bodyParser = require('body-parser');

var parameters = require('./config/parameters');


var mw = require('./services/middleware');
var db = require('./services/dbServices');


// controladores de la app
var loginCtr = require('./controllers/login.controller');


const jwt = require('jsonwebtoken');

const fs = require('fs');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

var router = express.Router();

router.post('/login', loginCtr.login);



var cert = fs.readFileSync('keyrs256.key.pub');



router.use(function(req, res, next){

    if(req.headers.authorization != undefined) {

        var token = req.headers.authorization.split(' ')[1];


        if (token) {
            jwt.verify(token, cert, function (err, decoded) {
                if (err) {
                    return res.json({codigo: -3, message: err});
                }
                else {
                    req.decoded = decoded;
                    next();
                }
            });
        }
        else {
            return res.status(403).send({
                codigo: -4,
                message: "Usuario Invalido"
            })
        }
    }
    else {
        res.write('<div>Juan Valdez Cafe</div>');
        res.end();
    }
});

router.post('/changePass', loginCtr.cambiarContrasena);

// router.get('/', function(req, res){
//     res.send('hello');
// });


app.use(router);

app.listen(parameters.serverPort,parameters.serverHost,null,function(){
   console.log('servidor iniciado');
   db.main.authenticate()
       .then(()=>{
            console.log("Servidor iniciado en " + parameters.serverHost + ":" + parameters.serverPort);
             // db.importar();
       })
       .catch( err =>{
           console.error("problemas para conectarse", err.message);
       });
});

const adminCtr = require('./controllers/admin.controller')(router);

router.post('/materiales', mw.consumirMateriales);

router.post('/salesOrder', mw.enviarOrden);

router.get('/codigos', mw.devolverCodigos);

router.post('/consecutivo',db.nextDoc);




// const http = require('http');
// var soap = require('soap');
// const hostname = '127.0.0.1';
// const port = 4700;
//
// const server = http.createServer(function(req, res) {
//    res.statusCode = 200;
//    res.setHeader('Content-Type', 'text/plain');
//    res.end('Hola Mundo');
// });
//
// server.listen(port, hostname, function() {
//     console.log("servidor iniciado");
// })
//
// var allowMethods = function(req, res, next){
//     res.header('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE, OPTIONS");
//     next();
// }

